FROM node:12.5.0-alpine AS build

ENV NODE_ENV=production
WORKDIR /app

RUN apk add --no-cache curl git && cd /tmp && \
    curl -#L https://github.com/tj/node-prune/releases/download/v1.0.1/node-prune_1.0.1_linux_amd64.tar.gz | tar -xvzf- && \
    mv -v node-prune /usr/local/bin && rm -rvf * && \
    echo "npm cache clean --force && node-prune" > /usr/local/bin/node-clean && chmod +x /usr/local/bin/node-clean

ADD package.json ./
ADD yarn.lock ./
RUN yarn && node-clean
ADD . ./
RUN yarn build

#### 2nd stage
FROM node:12.5.0-alpine

RUN apk --no-cache add curl bash

WORKDIR /app

ENV HOST=0.0.0.0

ADD package.json ./
ADD nuxt.config.js ./

COPY --from=builder ./app/server ./server/
COPY --from=builder ./app/node_modules ./node_modules/
COPY --from=builder ./app/.nuxt ./.nuxt/
COPY --from=builder ./app/static ./static/
# COPY --from=builder ./app/.env ./.env

EXPOSE 3000
CMD ["yarn", "start"]